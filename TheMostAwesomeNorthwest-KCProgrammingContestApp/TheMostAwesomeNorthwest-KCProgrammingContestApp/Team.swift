//
//  Team.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class Team: Equatable {
    var name: String
    var students: [String] = []
    
    init(name: String, students: [String]) {
        self.name = name
        self.students = students
    }
    
    // allows the ability to use == to compare 2 teams
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.name == rhs.name && lhs.students.elementsEqual(rhs.students)
    }
}
