//
//  ViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolsCellIdentifier", for: indexPath)
        cell.textLabel?.text = Schools.shared[indexPath.row].name
        cell.detailTextLabel?.text = Schools.shared[indexPath.row].coach
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // return the number of rows in our section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Schools.shared.numSchools()
    }
    
    // set the table to be editable
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            //tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
            Schools.shared.delete(school: Schools.shared[indexPath.row])
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Set the school used to pull in teams in the next window
        if segue.identifier == "schoolToTeamSegue" {
            let teamsTVC = segue.destination as! TeamsTableViewController
            teamsTVC.selectedSchool = Schools.shared[tableView.indexPathForSelectedRow!.row]
        }
    }
}

