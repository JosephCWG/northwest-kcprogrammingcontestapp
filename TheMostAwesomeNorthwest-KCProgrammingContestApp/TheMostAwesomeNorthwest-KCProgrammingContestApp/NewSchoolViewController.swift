//
//  NewSchoolViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var coachTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func done(_ sender: Any) {
        let name = nameTextField.text!
        let coach = coachTextField.text!
        // Add a new school to the singelton
        Schools.shared.add(school: School(name: name, coach: coach))
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
