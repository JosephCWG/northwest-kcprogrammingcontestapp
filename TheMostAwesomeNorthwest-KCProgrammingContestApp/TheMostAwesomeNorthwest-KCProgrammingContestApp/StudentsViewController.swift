//
//  StudentsViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {
    @IBOutlet weak var student1Label: UILabel!
    @IBOutlet weak var student2Label: UILabel!
    @IBOutlet weak var student3Label: UILabel!
    var team: Team?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set labels based on passed in team
        student1Label.text = team?.students[0]
        student2Label.text = team?.students[1]
        student3Label.text = team?.students[2]
    }
}
