//
//  School.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class School: Equatable {
    var name: String
    var coach: String
    var teams:[Team] = []
    
    // Add a team to the school
    func addTeam(name:String, students: [String]) {
        teams.append(Team(name: name, students: students))
    }
    
    // Initializer
    init(name: String, coach: String, teams: [Team]) {
        self.name = name
        self.coach = coach
        self.teams = teams
    }
    
    convenience init(name: String, coach: String) {
        self.init(name:name, coach:coach, teams: [])
    }
    
    // Even if you implement equatable in lower classes, you can't use the built in function elementsEqual w/o by
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name && lhs.coach == rhs.coach && lhs.teams.elementsEqual(rhs.teams, by: {(t1: Team, t2:Team) -> Bool in return t1 == t2})
    }
}
