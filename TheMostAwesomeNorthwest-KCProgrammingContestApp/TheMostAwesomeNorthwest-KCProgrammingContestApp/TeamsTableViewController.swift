//
//  TeamsTableViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class TeamAndMembersTableViewCell: UITableViewCell {
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
}

class TeamsTableViewController: UITableViewController {
    var selectedSchool: School?
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return selectedSchool!.teams.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeamAndMembersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "teamCellIdentifier") as! TeamAndMembersTableViewCell
        
        cell.topLabel.text = "Team Name: \(selectedSchool!.teams[indexPath.row].name)"
        cell.bottomLabel.text = "Members: \(selectedSchool!.teams[indexPath.row].students.joined(separator: ", "))"
        
        //cell?.textLabel?.text = selectedSchool!.teams[indexPath.row].name
        return cell//!
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "teamToStudentsSegue" {
            let studentsVC = segue.destination as! StudentsViewController
            //studentsVC.team! = selectedSchool!.teams[tableView.indexPathForSelectedRow!.row]
            //studentsVC.team = selectedSchool!.teams[0] WORKS
            studentsVC.team = selectedSchool!.teams[tableView.indexPathForSelectedRow!.row]
        } else if segue.identifier == "teamsToNewTeamSegue" {
            let newTeamVC = segue.destination as! NewTeamViewController
            newTeamVC.chosenSchool = selectedSchool!
        }
    }
}
