//
//  Schools.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class Schools {
    // setup shared singleton instance
    public static var shared = Schools();
    
    private var schools:[School] = []
    
    func numSchools() -> Int {
        return schools.count
    }
    
    func add(school:School) {
        schools.append(school)
    }
    
    func delete(school:School) {
        schools = schools.filter({ $0 !== school })
    }
    
    func deleteAt(index: Int) {
        schools.remove(at: index)
    }
    
    // ability to use Schools.shared[school_index]
    subscript(index:Int) -> School {
        return schools[index]
    }
    
    // prevent outside instantiation
    private init() {}
}
