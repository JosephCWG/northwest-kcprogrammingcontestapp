//
//  NewTeamViewController.swift
//  TheMostAwesomeNorthwest-KCProgrammingContestApp
//
//  Created by Watts,Joseph C on 3/7/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {
    var chosenSchool: School?
    
    @IBOutlet weak var teamNameTextField: UITextField!
    @IBOutlet weak var student1TextField: UITextField!
    @IBOutlet weak var student2TextField: UITextField!
    @IBOutlet weak var student3TextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func done(_ sender: Any) {
        let teamName = teamNameTextField.text!
        let student1Name = student1TextField.text!
        let student2Name = student2TextField.text!
        let student3Name = student3TextField.text!
        // Add a team to the passed in school
        chosenSchool!.addTeam(name: teamName, students: [student1Name, student2Name, student3Name])
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
